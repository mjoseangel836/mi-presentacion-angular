import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.css']
})
export class NgIfComponent implements OnInit {

  public calf = 70;

  public suma = 50;

  public multi = 100;

  public nombre = 'jose angel';

  constructor() { }

  ngOnInit(): void {
  }

}
